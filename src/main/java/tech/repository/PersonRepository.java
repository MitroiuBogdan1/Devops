package tech.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tech.entity.Person;

@Repository
@Transactional
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Modifying
    @Query(value = "UPDATE person SET team_id=null WHERE team_id = ?1", nativeQuery = true)
    void deleteTeamLink(Long teamID);

}
