package tech.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.entity.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {

}
