package tech.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@AllArgsConstructor

public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    private String name;


    @OneToMany(mappedBy = "team")
    private List<Person> personList = new ArrayList<>();


    public Team(String name) {
        System.out.println("Implementing awesome code1");
        System.out.println("implementing awesome code2");
        System.out.println("implementing asesome code 3");
        this.name = name;
    }


}
