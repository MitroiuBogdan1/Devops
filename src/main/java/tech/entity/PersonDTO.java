package tech.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDTO {

    public String name;
    public String teamName;

    public PersonDTO(String name) {
        this.name = name;
    }


}
