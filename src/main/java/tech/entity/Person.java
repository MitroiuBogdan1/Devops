package tech.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    public Person(String name, Team team) {
        this.name = name;
        this.team = team;
        System.out.println("TESSTIN git LAB");
    }

    public Person(String name) {
        this.name = name;
        System.out.println("hi4");
        System.out.println("hi6");
        System.out.println("hi6");
        System.out.println("hello from hello kitty");
        System.out.println("add some stuff 2");
    }
}
