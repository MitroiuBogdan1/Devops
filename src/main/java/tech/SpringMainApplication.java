package tech;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tech.entity.Person;
import tech.entity.Team;
import tech.repository.PersonRepository;
import tech.repository.TeamRepository;

@SpringBootApplication
public class SpringMainApplication implements CommandLineRunner {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringMainApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        teamRepository.save(new Team("team1"));
        teamRepository.save(new Team("team2"));
        teamRepository.save(new Team("team3"));

        personRepository.save(new Person("person1"));
        personRepository.save(new Person("person2"));
        personRepository.save(new Person("person3"));

        System.out.println("test1");
    }
}