package tech.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.entity.Person;
import tech.entity.PersonDTO;
import tech.entity.Team;
import tech.repository.PersonRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    TeamService teamService;

    public void savePerson(PersonDTO personDTO) {
        personRepository.save(new Person(personDTO.getName(), null));

    }

    public PersonDTO getPerson(Long personID) {
        Optional<Person> optionalPerson = personRepository.findById(personID);
        if (!optionalPerson.isPresent()) {
            return null;
        } else {
            if (optionalPerson.get().getTeam() == null) {
                return new PersonDTO(optionalPerson.get().getName());
            } else {
                return new PersonDTO(optionalPerson.get().getName(), optionalPerson.get().getTeam().getName());
            }
        }
    }


    public boolean deletePersonIfExists(Long personID) {
        Optional<Person> optionalPerson = personRepository.findById(personID);

        if (!optionalPerson.isPresent()) {
            return false;
        }
        personRepository.delete(optionalPerson.get());
        return true;
    }


    public boolean updatePersonIfExists(Long id, PersonDTO personDTO) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if (!optionalPerson.isPresent()) {
            return false;
        }
        optionalPerson.get().setName(personDTO.getName());
        personRepository.save(optionalPerson.get());
        return true;
    }

    public List<PersonDTO> getAllPersons() {
        Iterator<Person> personIterable = personRepository.findAll().iterator();
        List<PersonDTO> personDTOList = new ArrayList<>();

        while (personIterable.hasNext()) {
            Person tempPerson = personIterable.next();
            if (tempPerson.getTeam() == null) {
                personDTOList.add(new PersonDTO(tempPerson.getName()));
            } else {
                personDTOList.add(new PersonDTO(tempPerson.getName(), tempPerson.getTeam().getName()));
            }
        }
        return personDTOList;
    }


    public boolean addToTeamIfExists(Long teamID, Long personID) {
        Optional<Person> optionalPerson = personRepository.findById(personID);
        Team team = teamService.getTeamDAO(teamID);

        if (!optionalPerson.isPresent() || team == null) {
            return false;
        }
        optionalPerson.get().setTeam(team);
        personRepository.save(optionalPerson.get());
        return true;


    }


    public boolean removeTeamIfExist(Long personID) {
        Optional<Person> optionalPerson = personRepository.findById(personID);
        if (!optionalPerson.isPresent()) {
            return false;
        }
        optionalPerson.get().setTeam(null);
        personRepository.save(optionalPerson.get());
        return true;
    }


    public void removeTeamLinks(Long TeamID) {
        personRepository.deleteTeamLink(TeamID);

    }
}