package tech.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.entity.Person;
import tech.entity.PersonDTO;
import tech.entity.Team;
import tech.entity.TeamDTO;
import tech.repository.TeamRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PersonService personService;

    public TeamDTO getTeam(Long id) {
        Optional<Team> optionalPerson = teamRepository.findById(id);
        if (!optionalPerson.isPresent()) {
            return null;
        } else {
            return new TeamDTO(optionalPerson.get().getName(), getPersonDTOList(optionalPerson.get().getPersonList()));

        }
    }


    public List<TeamDTO> getAllTeams() {
        Iterator<Team> teamIterator = teamRepository.findAll().iterator();
        List<TeamDTO> teamDTOList = new ArrayList<>();

        while (teamIterator.hasNext()) {
            Team tempTeam = teamIterator.next();
            teamDTOList.add(new TeamDTO(tempTeam.getName(), getPersonDTOList(tempTeam.getPersonList())));
        }

        return teamDTOList;
    }


    public void addTeam(TeamDTO teamDTO) {
        teamRepository.save(new Team(teamDTO.getName()));
    }


    public boolean deleteTeamIfExists(Long teamID) {
        Optional<Team> optionalTeam = teamRepository.findById(teamID);

        if (!optionalTeam.isPresent()) {
            return false;
        }

        personService.removeTeamLinks(teamID);
        teamRepository.delete(optionalTeam.get());
        return true;
    }


    public Team getTeamDAO(Long id) {
        Optional<Team> optionalTeam = teamRepository.findById(id);
        if (!optionalTeam.isPresent()) {
            return null;
        } else {
            return optionalTeam.get();
        }
    }


    public boolean updateTeamIfExists(Long id, TeamDTO teamDTO) {
        Optional<Team> optionalTeam = teamRepository.findById(id);
        if (!optionalTeam.isPresent()) {
            return false;
        }
        optionalTeam.get().setName(teamDTO.getName());
        teamRepository.save(optionalTeam.get());
        return true;
    }

    public List<PersonDTO> getPersonsFromTeam(Long id) {
        Optional<Team> optionalTeam = teamRepository.findById(id);
        if (!optionalTeam.isPresent()) {
            return null;
        }
        return getPersonDTOList(optionalTeam.get().getPersonList());
    }

    private List<PersonDTO> getPersonDTOList(List<Person> list) {
        List<PersonDTO> personDTOList = new ArrayList<>();
        list.forEach(person -> {
            personDTOList.add(new PersonDTO(person.getName(), person.getTeam().getName()));
        });
        return personDTOList;
    }

}

