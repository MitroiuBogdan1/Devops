package tech.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.entity.TeamDTO;
import tech.exception.NotFoundException;
import tech.service.TeamService;

import java.net.URI;

@RestController
@RequestMapping(
        path = "/teams")
public class TeamController {

    @Autowired
    TeamService teamService;


    @GetMapping("/{id}")
    public ResponseEntity getTeam(@PathVariable Long id) {
        TeamDTO requestedTeam = teamService.getTeam(id);
        if (requestedTeam == null) {
            throw new NotFoundException("id-" + id);
        }
        return ResponseEntity.ok().body(requestedTeam);
    }


    @GetMapping("/{id}/persons")
    public ResponseEntity getPersonsFrom(@PathVariable Long id) {
        return ResponseEntity.ok().body(teamService.getPersonsFromTeam(id));
    }


    @GetMapping("/all")
    public ResponseEntity getAllTeams() {
        return ResponseEntity.ok().body(teamService.getAllTeams());
    }


    @PostMapping("/add")
    public ResponseEntity addTeam(@RequestBody TeamDTO teamDTO) {

        teamService.addTeam(teamDTO);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();
        return ResponseEntity.created(location).build();
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteTeam(@PathVariable Long id) {

        if (!teamService.deleteTeamIfExists(id)) {
            throw new NotFoundException("Team with id-" + id + " has not been found");
        }
        return ResponseEntity.ok("Team with id=" + id + " has been deleted");

    }


    @PutMapping("/update/{id}")
    public ResponseEntity updateTeam(@PathVariable Long id, @RequestBody TeamDTO teamDTO) {
        if (!teamService.updateTeamIfExists(id, teamDTO)) {
            throw new NotFoundException("Team with id-" + id + " has not been found");
        }
        return ResponseEntity.ok("Team with id=" + id + " has been updated");
    }


}
