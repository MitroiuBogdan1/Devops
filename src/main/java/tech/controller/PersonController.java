package tech.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.entity.PersonDTO;
import tech.exception.NotFoundException;
import tech.service.PersonService;
import tech.service.TeamService;

import java.net.URI;

@RestController
@RequestMapping(
        path = "/persons")
public class PersonController {

    @Autowired
    PersonService personService;

    @Autowired
    TeamService teamService;


    @GetMapping("/{personID}")
    public ResponseEntity getPerson(@PathVariable Long personID) {

        PersonDTO requestedPerson = personService.getPerson(personID);
        if (requestedPerson == null) {
            throw new NotFoundException("Person with personID-" + personID + " has not been found");
        }
        return ResponseEntity.ok().body(requestedPerson);
    }


    @GetMapping("/all")
    public ResponseEntity getAllPersons() {
        return ResponseEntity.ok().body(personService.getAllPersons());
    }


    @PostMapping("/add")
    public ResponseEntity createPerson(@RequestBody PersonDTO personDTO) {

        personService.savePerson(personDTO);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();
        return ResponseEntity.created(location).build();
    }


    @DeleteMapping("/delete/{personID}")
    public ResponseEntity deletePerson(@PathVariable Long personID) {
        if (!personService.deletePersonIfExists(personID)) {
            throw new NotFoundException("Person with personID-" + personID + " has not been found");
        }
        return ResponseEntity.ok("Person with personID=" + personID + " has been deleted");
    }


    @PutMapping("/update/{personID}")
    public ResponseEntity updatePerson(@PathVariable Long personID, @RequestBody PersonDTO personDTO) {
        if (!personService.updatePersonIfExists(personID, personDTO)) {
            throw new NotFoundException("Person with personID-" + personID + " has not been found or team dose not exist");
        }
        return ResponseEntity.ok("Person with personID=" + personID + " has been updated");
    }


    @PutMapping("/{personID}/addToTeam/{teamID}")
    public ResponseEntity addToTeam(@PathVariable Long teamID, @PathVariable Long personID) {
        if (!personService.addToTeamIfExists(teamID, personID)) {
            throw new NotFoundException("Person with personID-" + personID + " has not been found or team dosen`t exist");
        }
        return ResponseEntity.ok("Person with personID-" + personID + " has joined the team with teamID-" + teamID);
    }


    @PutMapping("{personID}/removeTeam")
    public ResponseEntity removeTeam(@PathVariable Long personID) {
        if (!personService.removeTeamIfExist(personID)) {
            throw new NotFoundException("Person with personID-" + personID + " has not been found or team dosen`t exist");
        }

        return ResponseEntity.ok("Person with personID-" + personID + " has left the team");
    }
}



