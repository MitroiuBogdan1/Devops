import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html'
})
export class ServerComponent implements OnInit {

  serverid: string = '9090';
  serverStatus: string = 'up';
  serverON: boolean = true;
  serverCreationStatus: string = 'No server was created'
  serverName: string;

  constructor() {
    setTimeout(() => {
      this.serverStatus = "OFF";
    }, 5000);
  }

  getServerStatus() {
    return this.serverStatus;
  }

  ngOnInit(): void {
  }

  onCreateServer() {
    this.serverCreationStatus = 'Server has been created!'
  }

  setServerName(event: any) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

}
